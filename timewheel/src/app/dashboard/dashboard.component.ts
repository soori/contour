import {
    Component, OnInit, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';
// import { Router } from '@angular/router';

import { DashBoardService } from './dashboard.service';
import { NgForm, FormBuilder, Validators, FormControl } from '@angular/forms';


@Component({
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
    public options: any = {}
    public chart: any;
    public dataloaded = false;
    public showerr = false;
    constructor(private dashboardService: DashBoardService) {
        this.options.title = 'Wheel Time';
        this.dashboardService.loadDashboards().
            subscribe(
            success => {
                if (success) {
                    this.options.data = [{
                        minPointSize: 2,
                        innerSize: '20%',
                        zMin: 35,
                        name: 'data',
                        data: success
                    }]
                }
            },
            error => {
                this.options.data = [{
                    minPointSize: 1,
                    innerSize: '10%',
                    zMin: 1,
                    name: 'data',
                    data: [{ "name": "CSR", "y": 10, "z": 7 }, { "name": "Clients", "y": 10, "z": 1 }, { "name": "Directs", "y": 10, "z": 4 }, { "name": "Fitness", "y": 10, "z": 6 }, { "name": "Friends & Family", "y": 10, "z": 0 }, { "name": "Industry", "y": 10, "z": 6 }, { "name": "Others", "y": 10, "z": 7 }, { "name": "Partners", "y": 10, "z": 7 }, { "name": "Reading ", "y": 10, "z": 8 }]
                }]
                this.dataloaded = true;
                if (this.options.data[0] && this.options.data[0].data[0]) {
                    console.log(this.options.data[0].data[0].name);
                    console.log(this.options.data[0].data[0].z);
                    if (this.options.data[0].data[0].z && this.options.data[0].data[0].z < 3) {
                        console.log('Am In');
                        this.showerr = true;
                    }
                }
                console.error('Dashbaord endpoint error', error);
            }
            );
    }


    ngOnDestroy() {
    }



    entries = [];
    selectedEntry: { [key: string]: any } = {
        title: null,
        imageUrl: null
    };


    ngOnInit() {
        this.entries = [
            {
                title: 'Happy',
                id: 1,
                imageUrl: 'assets/img/happy.png',
                class:'image'
            },
            {
                title: 'In different',
                id: 2,
                imageUrl: 'assets/img/confused.png',
                class:'image'
            },
            {
                title: 'Sad',
                id: 3,
                imageUrl: 'assets/img/sad.png',
                class:'image'
            },
            {
                title: 'Amused',
                id: 4,
                imageUrl: 'assets/img/amused.png',
                class:'image'
            },
            {
                title: 'Excited',
                id: 5,
                imageUrl: 'assets/img/excited.png',
                class:'image'
            },
            {
                title: 'Angry',
                id: 6,
                imageUrl: 'assets/img/angry.png',
                class:'image angry'
            }
        ];
        if (this.entries) {
            this.onSelectionChange(this.entries[0]);
        }

    }

    onSelectionChange(entry) {
        this.selectedEntry = Object.assign({}, this.selectedEntry, entry);
        console.log(this.selectedEntry);
    }
}

import { HeaderService } from './../header/header.service';
import {
    Component, OnInit, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';
import { Router } from '@angular/router';
import { User } from '@contour/angular';


@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnInit {
    public title: string;
    public user: User;

    public navInfo: any = {};
    public isResized: boolean = false;
    public profileInfo: any;


    constructor(private router: Router,
        private headerService: HeaderService) {
    }
    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        localStorage.getItem('user')
        this.title = 'Home';

        this.navInfo.custombar = true;
        this.navInfo.projectName = 'Life';
        this.navInfo.profile = true;
        this.navInfo.profileInfo = [
            {
                "code": "Signout",
                "name": "Dashboard",
                "url": "/apppath/dashboard",
                "items": []
            },
            {
                "code": "Signout",
                "name": "Team Report",
                "url": "/apppath/team",
                "items": []
            },
            {
                "code": "Signout",
                "name": "Scratch Pad",
                "url": "/apppath/form",
                "items": []
            },
            {
                "code": "Signout",
                "name": "Signout",
                "url": "/logout",
                "items": []
            }
        ];
        this.headerService.loadAll().then(
            data => {
                this.navInfo.navitems = data.navigation;
                this.navInfo.projectName = data.pagetitle;
                this.navInfo.custombar = data.custombar;
                this.navInfo.search = data.search;
                this.navInfo.favorites = data.favorites;
                this.navInfo.notifications = data.notifications;
                this.navInfo.profile = data.profile;
                this.navInfo.profileInfo = data.profileInfo;
                this.navInfo.variantsInfo = data.variantsInfo;
                this.navInfo.favoritesInfo = data.favoritesInfo;
            },
            err => {
                this.navInfo.navitems = [];
                this.navInfo.projectName = 'Life';
                this.navInfo.custombar = true;
                this.navInfo.search = false;
                this.navInfo.favorites = false;
                this.navInfo.notifications = false;
                this.navInfo.profile = true;
                this.navInfo.profileInfo = [
                    {
                        "code": "Signout",
                        "name": "Dashboard",
                        "url": "/apppath/dashboard",
                        "items": []
                    },
                    {
                        "code": "Signout",
                        "name": "Capture",
                        "url": "/apppath/form",
                        "items": []
                    },
                    {
                        "code": "Signout",
                        "name": "My ScratchPad",
                        "url": "/apppath/myscratchpad",
                        "items": []
                    },
                    {
                        "code": "Signout",
                        "name": "Team ScratchPad",
                        "url": "/apppath/teammyscratchpad",
                        "items": []
                    },
                    {
                        "code": "Signout",
                        "name": "Signout",
                        "url": "/logout",
                        "items": []
                    }
                ];
                this.navInfo.variantsInfo = [];
                this.navInfo.favoritesInfo = [];

                console.log('Error', err);
            }
        );
    }
}

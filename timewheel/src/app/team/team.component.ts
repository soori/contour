import {
    Component, OnInit, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';
// import { Router } from '@angular/router';

import { NgForm, FormBuilder, Validators, FormControl } from '@angular/forms';

import { TeamService } from './team.service';


@Component({
    selector: 'dashboard',
    templateUrl: 'team.component.html'
})

export class TeamComponent implements OnInit {
    public options: any = {}
    public chart: any;
    public dataloaded = false;
    public showerr = false;
    constructor(private dashboardService: TeamService) {
        this.options.title = 'Wheel Time';
        this.dashboardService.loadDashboards().
            subscribe(
            success => {
                if (success) {
                    this.options.data = [{
                        minPointSize: 2,
                        innerSize: '20%',
                        zMin: 35,
                        name: 'data',
                        data: success
                    }]
                }
            },
            error => {
                this.options.data = [{
                    minPointSize: 1,
                    innerSize: '10%',
                    zMin: 1,
                    name: 'data',
                    data: [{ "name": "CSR", "y": 10, "z": 7 }, { "name": "Clients", "y": 10, "z": 1 }, { "name": "Directs", "y": 10, "z": 4 }, { "name": "Fitness", "y": 10, "z": 6 }, { "name": "Friends & Family", "y": 10, "z": 0 }, { "name": "Industry", "y": 10, "z": 6 }, { "name": "Others", "y": 10, "z": 7 }, { "name": "Partners", "y": 10, "z": 7 }, { "name": "Reading ", "y": 10, "z": 8 }]
                }]
                this.dataloaded = true;
                if (this.options.data[0] && this.options.data[0].data[0]) {
                    console.log(this.options.data[0].data[0].name);
                    console.log(this.options.data[0].data[0].z);
                    if (this.options.data[0].data[0].z && this.options.data[0].data[0].z < 3) {
                        console.log('Am In');
                        this.showerr = true;
                    }
                }
                console.error('Dashbaord endpoint error', error);
            }
            );
    }

    ngOnInit() {

    }

    ngOnDestroy() {
    }
}

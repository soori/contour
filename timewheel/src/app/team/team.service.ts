import { User } from '@contour/angular';
import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class TeamService {
    public user: User;
    constructor(private http: Http) {
        this.user = JSON.parse(localStorage.getItem('user'));
    }

  

    // Load dash board cards
    loadChartData() {
        return this.http.get('http://10.0.2.2:8080/timesheet/'+this.user.userId+'/dashboard')
            .map(this.handleSuccess);
    }

    handleSuccess(response: any) {
        return response.json();
    }

    handleError(response: any) {
        console.log('response', response);
    }

      loadDashboards(): Observable<any> {
        return this.http
            .get('http://localhost:8080/timesheet/'+this.user.userId+'/dashboard')
            .map((res: Response) => res.json())
            .catch((error: any) => 
            Observable.throw(error.json() || 'Server error'));
    }
}
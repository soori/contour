import { TeamComponent } from './team/team.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guards/AuthGuard';
import { DashboardComponent } from './dashboard/dashboard.component';

import { FormComponent } from './form/form.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LoginComponent },

  {
    path: 'apppath',
    component: HomeComponent,
    children: [
      {
        path: '', redirectTo: 'form', pathMatch: 'full'
      },
      {
        path: 'dashboard', component: DashboardComponent
      },
      {
        path: 'team', component: TeamComponent
      },
      {
        path: 'form', component: FormComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

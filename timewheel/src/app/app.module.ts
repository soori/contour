import { DashboardComponent } from './dashboard/dashboard.component';
// Angular Modules
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule, } from '@angular/platform-browser';
import { HttpModule, Http, BaseRequestOptions } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { HomeComponent } from './home/home.component';

import { HeaderService } from './header/header.service';
import { AuthenticationService } from './login/authentication.service';

import { AuthGuard } from './guards/AuthGuard';
// contour
import { ContourModule } from './contour.module';
import { ContourGridModule } from './grid.module';
// thirdparty specific to project
import { LoginComponent } from './login/login.component';

import { APP_CONFIG, AppConfig } from './config/app.config';
import { FormComponent } from './form/form.component';
import { DashBoardService } from './dashboard/dashboard.service';
import { FormService } from './form/form.service';
import {MatCardModule , MatButtonModule, MatCheckboxModule, MatInputModule,
   MatButtonToggleModule, MatTabsModule, MatFormFieldModule } from '@angular/material';

import { ContourPieChartModule, ContourVariablePiechartModule } from '@contour/charts';
import { TeamComponent } from './team/team.component';
import { TeamService } from './team/team.service';




@NgModule({
  imports: [
    ContourModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    ContourPieChartModule,
    ContourVariablePiechartModule,
    MatInputModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    AppRoutingModule,
  ],
  declarations: [
    HomeComponent,
    LoginComponent,
    FormComponent,
    DashboardComponent,
    TeamComponent,
    AppComponent,
  ],
  exports: [
  ],
  providers: [
    HeaderService,
    DashBoardService,
    FormService,
    TeamService,
    AuthenticationService,
    DatePipe,
    AuthGuard,
    BaseRequestOptions,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    { provide: APP_CONFIG, useValue: AppConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

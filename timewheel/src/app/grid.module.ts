// Angular Modules
import { NgModule, ModuleWithProviders } from '@angular/core';
import { BrowserModule, } from '@angular/platform-browser';
import { HttpModule, Http, BaseRequestOptions } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { DialogComponent } from '@contour/angular';

// KendoGrid Reference

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
    ],
    declarations: [
        DialogComponent
    ],
    exports: [
        DialogComponent
    ]
})
export class ContourGridModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ContourGridModule,
            providers: [
            ]
        };
    }
}

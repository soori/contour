import { Injectable } from '@angular/core';
import { Route, Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('user')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }

    canLoad(route: Route): boolean {
        if (localStorage.getItem('user')) {
            return true;
        }
        return false;
    }
}

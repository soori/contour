import { NgModule, ModuleWithProviders } from '@angular/core';
import { BrowserModule, } from '@angular/platform-browser';
import { HttpModule, Http, BaseRequestOptions } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { CommonModule } from '@angular/common';


@NgModule({
    imports: [BrowserModule,
        FormsModule,
        HttpModule, CommonModule, MatButtonModule, MatCheckboxModule, MatInputModule],
    exports: [MatButtonModule, MatCheckboxModule, MatInputModule],
})
export class MaterialModule { }
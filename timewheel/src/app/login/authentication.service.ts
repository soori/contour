import { Injectable, Inject } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { APP_CONFIG, IAppConfig } from '../config/app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { User } from '@contour/angular';


@Injectable()
export class AuthenticationService {

    public getUserProfile: string;
    constructor(private http: Http, @Inject(APP_CONFIG) private config: IAppConfig) {
    }

    logout() {
        localStorage.removeItem('user');
        localStorage.clear();
    }

    authenticate(username: string, passwrod: string): Observable<any> {
        let user: User = new User();


        if (username && username.length > 4) {
            if (username === 'admin') {
                user.userId = '1';
                user.firstName = 'Admin';
            }
            if (username === 'niladri') {
                user.userId = '2';
                user.firstName = 'Niladri';
            }
            if (username === 'leela') {
                user.userId = '3';
                user.firstName = 'Leela';
            }
            if (username === 'pavan') {
                user.userId = '4';
                user.firstName = 'Pavan';
            }
            if (username === 'laxmikanth') {
                user.userId = '5';
                user.firstName = 'Laxmikanth';
            }
            localStorage.setItem('user', JSON.stringify(user));
            return Observable.of('/apppath');
        }
    }


}


import { AuthenticationService } from '../login/authentication.service';
import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { NgClass } from '@angular/common';

@Component({
    selector: 'login-cmp',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {
    model: any = {};
    returnUrl: string;
    loginFail: boolean;
    constructor(private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService) {
    }

    ngOnInit() {
        this.authenticationService.logout();
    }

    login() {
        this.authenticationService.authenticate(this.model.username, this.model.password).
            subscribe(
            success => {
                if (success) {
                    this.router.navigateByUrl(success);
                }
            },
            error => {
                this.loginFail = true;
                console.log('Login fails');
            }
            );
    }

}

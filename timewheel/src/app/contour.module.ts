import { RouterModule } from '@angular/router';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule, } from '@angular/platform-browser';
import { HttpModule, Http, BaseRequestOptions } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


// Components
import { GroupByPipe } from './pipes/group-by.pipe';
import { CommonModule } from '@angular/common';

// Directives
// From contour
import { HeaderComponent } from '@contour/angular';
import { FooterComponent } from '@contour/angular';
import { NotificationComponent } from '@contour/angular';
import { NotificationbellComponent } from '@contour/angular';
import { OnTrackCardComponent } from '@contour/angular';
import { ProfileCardComponent } from '@contour/angular';
import {
    CardComponent, CardTitle, CardIcon, CardContent,
    CardStutus, CardHeader, CardFooter, CardActions, CardSmIcon, CardMdIcon, CardLgIcon, CardSubTitle, CardModal
} from '@contour/angular';

import { FavoritesComponent } from '@contour/angular';
import { SearchComponent } from '@contour/angular';
import { ProfileComponent, MegamenuComponent } from '@contour/angular';
import { LoadUserVariantsComponent } from '@contour/angular';

// Services
import { HeaderService } from './header/header.service';

import { APP_CONFIG, AppConfig } from './config/app.config';

import { AuthenticationService } from './login/authentication.service';
import { AuthGuard } from './guards/AuthGuard';



@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CommonModule,
        RouterModule,
        BrowserAnimationsModule,
    ],
    declarations: [
        HeaderComponent,
        CardComponent, CardTitle, CardIcon, CardContent, CardStutus, CardHeader, CardFooter, CardActions,
        CardSmIcon, CardMdIcon, CardLgIcon, CardSubTitle, CardModal,
        OnTrackCardComponent,
        MegamenuComponent,
        ProfileCardComponent,
        FooterComponent,
        NotificationComponent,
        NotificationbellComponent,
        GroupByPipe,
        FavoritesComponent,
        SearchComponent,
        ProfileComponent,
        LoadUserVariantsComponent
    ],
    exports: [
        HeaderComponent,
        CardComponent, CardTitle, CardIcon, CardContent, CardStutus, CardHeader, CardFooter, CardActions,
        CardSmIcon, CardMdIcon, CardLgIcon, CardSubTitle, CardModal,
        OnTrackCardComponent,
        ProfileCardComponent,
        FooterComponent,
        NotificationComponent,
        NotificationbellComponent,
        GroupByPipe,
        FavoritesComponent,
        SearchComponent,
        ProfileComponent,
        LoadUserVariantsComponent
    ],
    providers: [
    ],
    bootstrap: []
})
export class ContourModule { }

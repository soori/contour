import {
    Component, OnInit, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';
// import { Router } from '@angular/router';

import { ScratchPadService } from './scratchpad.service';
import { NgForm, FormBuilder, Validators, FormControl } from '@angular/forms';


@Component({
    selector: 'scratchPad',
    templateUrl: 'scratchPad.component.html',
    styleUrls: ['scratchPad.component.scss']
})

export class ScratchPadComponent implements OnInit {
    entries = [];
    selectedEntry: { [key: string]: any } = {
        title: null,
        imageUrl: null
    };
    constructor(private dashboardService: ScratchPadService) {
    }

    ngOnDestroy() {
    }

    ngOnInit() {
        this.entries = [
            {
                title: 'Happy',
                id: 1,
                imageUrl: 'assets/img/happy.png',
                class: 'image'
            },
            {
                title: 'In different',
                id: 2,
                imageUrl: 'assets/img/confused.png',
                class: 'image'
            },
            {
                title: 'Sad',
                id: 3,
                imageUrl: 'assets/img/sad.png',
                class: 'image'
            },
            {
                title: 'Amused',
                id: 4,
                imageUrl: 'assets/img/amused.png',
                class: 'image'
            },
            {
                title: 'Excited',
                id: 5,
                imageUrl: 'assets/img/excited.png',
                class: 'image'
            },
            {
                title: 'Angry',
                id: 6,
                imageUrl: 'assets/img/angry.png',
                class: 'image angry'
            }
        ];
        if (this.entries) {
            this.onSelectionChange(this.entries[0]);
        }
    }

    onSelectionChange(entry) {
        this.selectedEntry = Object.assign({}, this.selectedEntry, entry);
        console.log(this.selectedEntry);
    }
}

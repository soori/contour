import {
    Component, OnInit, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';
// import { Router } from '@angular/router';

import { AdminScratchPadService } from './admin-scratchpad.service';
import { NgForm, FormBuilder, Validators, FormControl } from '@angular/forms';


@Component({
    selector: 'admin-scratchpad',
    templateUrl: 'admin-scratchpad.component.html',
    styleUrls: ['admin-scratchpad.component.scss']
})

export class AdminScratchpadComponent implements OnInit {
    entries = [];
    selectedEntry: { [key: string]: any } = {
        title: null,
        imageUrl: null
    };
    constructor(private dashboardService: AdminScratchPadService) {
    }

    ngOnDestroy() {
    }
    ngOnInit() {
        this.entries = [
            {
                title: 'Happy',
                id: 1,
                imageUrl: 'assets/img/happy.png',
                class: 'image',
                badge:11
            },
            {
                title: 'In different',
                id: 2,
                imageUrl: 'assets/img/confused.png',
                class: 'image',
                badge:0
            },
            {
                title: 'Sad',
                id: 3,
                imageUrl: 'assets/img/sad.png',
                class: 'image',
                badge:2
            },
            {
                title: 'Amused',
                id: 4,
                imageUrl: 'assets/img/amused.png',
                class: 'image',
                badge:4
            },
            {
                title: 'Excited',
                id: 5,
                imageUrl: 'assets/img/excited.png',
                class: 'image',
                badge:10
            },
            {
                title: 'Angry',
                id: 6,
                imageUrl: 'assets/img/angry.png',
                class: 'image angry',
                badge:6
            }
        ];
    }

}

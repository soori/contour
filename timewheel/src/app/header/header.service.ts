import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { APP_CONFIG, IAppConfig } from '../config/app.config';


@Injectable()
export class HeaderService {
    constructor(private http: Http, @Inject(APP_CONFIG) private config: IAppConfig
    ) {
    }
    loadAll() {
        return this.http.get('../' + this.config.contextpath + 'mock-data/navigation.json')
            .toPromise()
            .then(this.handleSuccess)
            .catch(this.handleError);
    }
    handleSuccess(response: any) {
        return response.json();
    }
    handleError(response: any) {
        console.log('response', response);
    }
}

import {
    Component, OnInit, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';
// import { Router } from '@angular/router';

import { FormService } from './form.service';
import { NgForm, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'form-cmp',
    templateUrl: 'form.component.html',
    styleUrls: ['form.component.scss']
})

export class FormComponent implements OnInit {
    model: any = {};
    public loader: boolean;
    constructor(private formService: FormService) {

    }
    ngOnInit() {
        this.loader = false;
         this.entries = [
            {
                title: 'Happy',
                id: 1,
                imageUrl: 'assets/img/happy.png',
                class: 'image'
            },
            {
                title: 'In different',
                id: 2,
                imageUrl: 'assets/img/confused.png',
                class: 'image'
            },
            {
                title: 'Sad',
                id: 3,
                imageUrl: 'assets/img/sad.png',
                class: 'image'
            }
        ];
        if (this.entries) {
            this.onSelectionChange(this.entries[0]);
        }
    }
    ngOnDestroy() {
    }


    save() {
        console.log(this.model);
        this.formService.submitForm(this.model).subscribe(
            data => {
                console.log('submitted successfully');
            },
            err => { console.log('Error', err); }
        );

    }

       entries = [];
    selectedEntry: { [key: string]: any } = {
        title: null,
        imageUrl: null
    };
 
   

    onSelectionChange(entry) {
        this.selectedEntry = Object.assign({}, this.selectedEntry, entry);
        console.log(this.selectedEntry);
    }
}

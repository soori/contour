import { User } from '@contour/angular';
import { Injectable, Inject } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class FormService {
    public user: User;
    constructor(private http: Http) {
        this.user = JSON.parse(localStorage.getItem('user'));
    }
    public submitForm(body: any) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post('http://localhost:8080/timesheet/' + this.user.userId + '/submit', body, options)
            .map(
            res => {
                return res.json();
            }
            ).catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }
}
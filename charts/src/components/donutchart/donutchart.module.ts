import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContourDonutChartComponent } from './donutchart.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ContourDonutChartComponent
  ],
  exports: [
    ContourDonutChartComponent
  ]
})
export class  ContourDonutChartModule  {
}

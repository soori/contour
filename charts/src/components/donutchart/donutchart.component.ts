import { Component, OnInit, OnChanges, Input, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    moduleId: module.id,
    selector: 'contour-donutchart-cmp',
    templateUrl: 'donutchart.component.html',
    styleUrls: ['donutchart.component.scss'],
})

export class ContourDonutChartComponent implements OnInit, OnChanges {

    @Input() options: any;
    @ViewChild('container') chartContainer: any;
    public chartType: string;
    public seriesData: any;
    public title: string;
    constructor(public el: ElementRef) { }

    ngOnInit() {
    }
    ngOnChanges() {
        // Get the Chat Values from @input variable
        this.chartType = this.options.chartType;
        this.seriesData = this.options.data;
        this.title = this.options.title;
        console.log('chart option' + this.options);

        // Create the chart
        Highcharts.chart(this.chartContainer.nativeElement, {
            chart: {
                type: 'pie'
            },
            title: {
                text: this.title
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%']
                }
            },
            tooltip: {
                valueSuffix: '%'
            },
            series: [ {
                name: 'Versions',
                data: this.seriesData,
                size: '80%',
                innerSize: '60%',

            }]

        });
    }

}

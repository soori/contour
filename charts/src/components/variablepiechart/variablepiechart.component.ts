import { Component, OnInit, OnChanges, Input, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import * as Highcharts from 'highcharts';
declare var require: any;
require('highcharts/modules/variable-pie')(Highcharts);
require('highcharts/modules/exporting')(Highcharts);


@Component({
    moduleId: module.id,
    selector: 'contour-variablepiechart-cmp',
    templateUrl: 'variablepiechart.component.html',
    styleUrls: ['variablepiechart.component.scss'],
})

export class ContourVariablePiechartComponent implements OnInit, OnChanges {

    @Input() options: any;
    @ViewChild('container') chartContainer: any;
    public chartType: string;
    public seriesData: any;
    public title: string;
    constructor(public el: ElementRef) {

    }

    ngOnInit() {

    }
    ngOnChanges() {

        // Get the Chat Values from @input variable
        this.chartType = this.options.chartType;
        this.seriesData = this.options.data;
        this.title = this.options.title;
         console.log('chart option' + JSON.stringify(this.options));

        Highcharts.chart(this.chartContainer.nativeElement, {
            chart: {
                type: 'variablepie'
            },
            title: {
                text: this.title
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
                              'Time spent in Hrs: <b>{point.z}</b><br/>'
            },
            series: this.seriesData
        });
    }



}

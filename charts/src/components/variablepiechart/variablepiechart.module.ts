import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContourVariablePiechartComponent } from './variablepiechart.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ContourVariablePiechartComponent
  ],
  exports: [
    ContourVariablePiechartComponent
  ]
})
export class  ContourVariablePiechartModule  {
}

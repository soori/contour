import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContourSemiDonutChartComponent } from './semidonutchart.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ContourSemiDonutChartComponent
  ],
  exports: [
    ContourSemiDonutChartComponent
  ]
})
export class  ContourSemiDonutChartModule  {
}

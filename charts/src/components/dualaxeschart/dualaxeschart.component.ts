import { Component, OnInit, OnChanges, Input, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import * as Highcharts from 'highcharts';
declare var require: any;
require('highcharts/modules/annotations')(Highcharts);

@Component({
    moduleId: module.id,
    selector: 'contour-dualaxeschart-cmp',
    templateUrl: 'dualaxeschart.component.html',
    styleUrls: ['dualaxeschart.component.scss'],
})

export class ContourDuelAxesChartComponent implements OnInit, OnChanges {

    @Input() options: any;
    @ViewChild('container') chartContainer: any;
    public chartType: string;
    public seriesData: any;
    public title: string;
    constructor(public el: ElementRef) { }

    ngOnInit() {
    }
    ngOnChanges() {
        // Get the Chat Values from @input variable
        this.seriesData = this.options.data;
        this.title = this.options.title;
        console.log('chart option' + this.options);

        Highcharts.chart(this.chartContainer.nativeElement, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Domestric Deliver Fails Analysis'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['Jan-17', 'Feb-17', 'Mar-17', 'Apr-17', 'May-17', 'Jun-17'],
                crosshair: true,
                max: 5
            }],
            yAxis: [{ // Primary yAxis
                tickInterval: 2000000,
                labels: {
                    format: '${value}',
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: '#89A54E'
                    }
                }
            }, { // Secondary yAxis
                tickInterval: 20, // set ticks to 20
                min: 0,
                max: 100,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: '#4572A7'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true
                //backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            // annotations: [{
            //     labelOptions: {
            //         backgroundColor: 'rgba(255,255,255,0.5)',
            //         verticalAlign: 'top',
            //         y: 15
            //     },
            //     labels: [   {
            //         point: {
            //             x: 10,
            //             y: 1
            //         },
            //         text: 'x: {point.plotX} px<br/>y: {point.plotY} px'
            //     }],
            //     zIndex : 10
            // }],
            series: [{
                name: 'Domestric COD>5 days($ Value)',
                type: 'column',
                yAxis: 1,
                data: [2, 30, 15, 60, 80, 4]
            }, {
                name: 'Domestric COD>5 days(#s)',
                color: '#C61D1D',
                type: 'spline',
                data: [2500000, 3500000, 4500000, 1000000, 6500000, 1800000]
            }]
        });
    }

}

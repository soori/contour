import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContourDuelAxesChartComponent } from './dualaxeschart.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ContourDuelAxesChartComponent
  ],
  exports: [
    ContourDuelAxesChartComponent
  ]
})
export class  ContourDualaxeschartChartModule  {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContourVariablePieChartComponent } from './variablepiechart.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ContourVariablePieChartComponent
  ],
  exports: [
    ContourVariablePieChartComponent
  ]
})
export class  ContourVariablePieChartModule  {
}

import { Component, OnInit, OnChanges, Input, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    moduleId: module.id,
    selector: 'contour-variable-piechart-cmp',
    templateUrl: 'variablepiechart.component.html',
    
    styleUrls: ['variablepiechart.component.scss'],
})

export class ContourVariablePieChartComponent implements OnInit, OnChanges {

    @Input() options: any;
    @ViewChild('containerew') chartContainer: any;
    public seriesData: any;
    public title: string;
    constructor(public el: ElementRef) { }

    ngOnInit() {
    }
    ngOnChanges() {

        this.seriesData = this.options.data;
        this.title = this.options.title;

        console.log(' variabel pie chart option', this.options);

        Highcharts.chart(this.chartContainer.nativeElement, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'variablepie'
            },
            title: {
                text: this.title
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
                'Time spent in Hrs: <b>{point.z}</b><br/>'
            },
            series: [{
                innerSize: '20%',
                name: 'countries',
                data: this.seriesData
            }]
        });
    }

}

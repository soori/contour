import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContourPieChartComponent } from './piechart.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ContourPieChartComponent
  ],
  exports: [
    ContourPieChartComponent
  ]
})
export class  ContourPieChartModule  {
}

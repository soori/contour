import { Component, OnInit, OnChanges, Input, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    moduleId: module.id,
    selector: 'contour-piechart-cmp',
    templateUrl: 'piechart.component.html',
    styleUrls: ['piechart.component.scss'],
})

export class ContourPieChartComponent implements OnInit, OnChanges {

    @Input() options: any;
    @ViewChild('container') chartContainer: any;
    public chartType: string;
    public seriesData: any;
    public title: string;
    constructor(public el: ElementRef) { }

    ngOnInit() {
    }
    ngOnChanges() {
        // Get the Chat Values from @input variable
        this.chartType = this.options.chartType;
        this.seriesData = this.options.data;
        this.title = this.options.title;
        console.log('chart option' + this.options);

        Highcharts.chart(this.chartContainer.nativeElement, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Browser market shares January, 2015 to May, 2015'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Brands',
                data: this.seriesData
            }]
        });
    }

}

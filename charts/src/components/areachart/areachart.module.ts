import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContourAreaChartComponent } from './areachart.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ContourAreaChartComponent
  ],
  exports: [
    ContourAreaChartComponent
  ]
})
export class  ContourAreaChartModule  {
}

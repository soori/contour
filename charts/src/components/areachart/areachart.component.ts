import { Component, OnInit, OnChanges, Input, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    moduleId: module.id,
    selector: 'contour-areachart-cmp',
    templateUrl: 'areachart.component.html',
    styleUrls: ['areachart.component.scss'],
})

export class ContourAreaChartComponent implements OnInit, OnChanges {

    @Input() options: any;
    @ViewChild('container') chartContainer: any;
    public chartType: string;
    public seriesData: any;
    public title: string;
    constructor(public el: ElementRef) { }

    ngOnInit() {
    }
    ngOnChanges() {
        // Get the Chat Values from @input variable
        this.seriesData = this.options.data;
        this.title = this.options.title;
        console.log('chart option' + this.options);

        Highcharts.chart(this.chartContainer.nativeElement, {
            chart: {
                type: 'area'
            },
            title: {
                text:  this.title
            },
            xAxis: {
                allowDecimals: false,
                labels: {
                    formatter: function () {
                        return this.value; // clean, unformatted number for year
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Nuclear weapon states'
                },
                labels: {
                    formatter: function () {
                        return this.value / 1000 + 'k';
                    }
                }
            },
            tooltip: {
                pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: this.seriesData
        });
    }

}

import { Component, OnInit, OnChanges, Input, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    moduleId: module.id,
    selector: 'contour-linechart-cmp',
    templateUrl: 'linechart.component.html',
    styleUrls: ['linechart.component.scss'],
})

export class ContourLineChartComponent implements OnInit, OnChanges {

    @Input() options: any;
    @ViewChild('container') chartContainer: any;
    public chartType: string;
    public seriesData: any;
    public title: string;
    constructor(public el: ElementRef) { }

    ngOnInit() {
    }
    ngOnChanges() {
        // Get the Chat Values from @input variable
        this.chartType = this.options.chartType;
        this.seriesData = this.options.data;
        this.title = this.options.title;
        console.log('chart option' + this.options);

        Highcharts.chart(this.chartContainer.nativeElement, {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Solar Employment Growth by Sector, 2010-2016'
            },
            subtitle: {
                text: 'Source: thesolarfoundation.com'
            },
            yAxis: {
                title: {
                    text: 'Number of Employees'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    pointStart: 2010
                }
            },
            series: this.seriesData
        });
    }

}

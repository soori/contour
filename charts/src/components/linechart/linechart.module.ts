import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContourLineChartComponent } from './linechart.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ContourLineChartComponent
  ],
  exports: [
    ContourLineChartComponent
  ]
})
export class  ContourLineChartModule  {
}

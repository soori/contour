import { MockDataService } from './service/mockdata.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';

import { ContourLineChartComponent } from '../../src/components';
import { ContourAreaChartComponent } from '../../src/components';
import { ContourPieChartComponent } from '../../src/components';
import { ContourDonutChartComponent } from '../../src/components';
import { ContourSemiDonutChartComponent } from '../../src/components';
import { ContourVariablePiechartComponent } from '../../src/components';
import { ContourDuelAxesChartComponent } from '../../src/components';

import { LinechartExampleComponent } from './linechartexample/linechartexample.component';
import { AreachartExampleComponent } from './areachartexample/areachartexample.component';
import { PiechartExampleComponent } from './piechartexample/piechartexample.component';
import { SemiDonutchartExampleComponent } from './semidonutchartexample/semidonutchartexample.component';
import { VariablePiechartExampleComponent } from './variablepiechartexample/variablepiechartexample.component';
import { DuelAxeschartExampleComponent } from './dualaxeschartexample/dualaxeschartexample.component';

@NgModule({
  declarations: [
    AppComponent,
    ContourLineChartComponent,
    ContourAreaChartComponent,
    ContourPieChartComponent,
    ContourDonutChartComponent,
    ContourSemiDonutChartComponent,
    ContourVariablePiechartComponent,
    ContourDuelAxesChartComponent,

    LinechartExampleComponent,
    AreachartExampleComponent,
    PiechartExampleComponent,
    SemiDonutchartExampleComponent,
    VariablePiechartExampleComponent,
    DuelAxeschartExampleComponent
    ],
  imports: [
    BrowserModule, HttpModule, AppRoutingModule,
    FormsModule,
    CommonModule
  ],
  providers: [MockDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }

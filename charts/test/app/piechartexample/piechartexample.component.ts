import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'pie-chart-example',
    templateUrl: './piechartexample.component.html'
})
export class PiechartExampleComponent implements OnInit {
    public options: any;
    constructor() {
    }
    ngOnInit() {
        this.options = {
            title: 'Assigned',
            data:  [{
                name: 'IE',
                y: 56.33
            }, {
                name: 'Chrome',
                y: 24.03,
                sliced: true,
                selected: true
            }, {
                name: 'Firefox',
                y: 10.38
            }, {
                name: 'Safari',
                y: 4.77
            }, {
                name: 'Opera',
                y: 0.91
            }, {
                name: 'Other',
                y: 0.2
            }]

        };

    }

}

import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class MockDataService {


    constructor(private http: Http) {
    }

    getHeader() {
        return this.http
            .get('mock-data/header.json')
            .toPromise()
            .then(this.handleSuccess)
            .catch(this.handleError);
    }

    handleSuccess(response: any) {
        return response.json();
    }
    handleError(response: any) {
        console.log('response', response)
    }
}
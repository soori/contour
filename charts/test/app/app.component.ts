import { Component, OnInit } from '@angular/core';
import { MockDataService } from './service/mockdata.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: string;
  header: any;

  constructor(private moMockDataService: MockDataService) {

  }
  ngOnInit() {
 
  }

}

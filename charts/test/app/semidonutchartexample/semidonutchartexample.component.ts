import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'semidonut-chart-example',
    templateUrl: './semidonutchartexample.component.html'
})
export class SemiDonutchartExampleComponent implements OnInit {
    public options: any;
    constructor() {
    }
    ngOnInit() {
        this.options = {
            title: 'Assigned',
            data:  [{
                name: 'Completed',
                y: 50,
                color: '#3cb541'
            }, {
                name: 'In Progress',
                y: 10,
                color: '#00a3d6'
            },
            {
                name: 'Yet to start',
                y: 20,
                color: '#ebf4f7'
            }]

        };

    }

}

import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LinechartExampleComponent } from './linechartexample/linechartexample.component';
import { AreachartExampleComponent } from './areachartexample/areachartexample.component';
import { PiechartExampleComponent } from './piechartexample/piechartexample.component';
import { SemiDonutchartExampleComponent } from './semidonutchartexample/semidonutchartexample.component';
import { VariablePiechartExampleComponent } from './variablepiechartexample/variablepiechartexample.component';
import { DuelAxeschartExampleComponent } from './dualaxeschartexample/dualaxeschartexample.component';


const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: AppComponent },
    { path: 'linechart', component: LinechartExampleComponent },
    { path: 'areachart', component: AreachartExampleComponent },
    { path: 'piechart', component: PiechartExampleComponent },
    { path: 'semidonutchart', component: SemiDonutchartExampleComponent },
    { path: 'variablepiechart', component: VariablePiechartExampleComponent },
    { path: 'dualaxeschart', component: DuelAxeschartExampleComponent }

];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { useHash: true })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }

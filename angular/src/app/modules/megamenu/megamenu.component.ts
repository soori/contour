import { Component, Input, Output, EventEmitter  } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'megamenu-comp',
    templateUrl: 'megamenu.component.html',
    styleUrls: ['megamenu.component.scss']
})

export class MegamenuComponent {
    @Input()
    headerInfo: any;
    @Output()
    handleclick = new EventEmitter<any>();
    router: Router;

    constructor(router: Router) {
        this.router = router;
    }

public onMenuChildClick(value: any) {
        // this.router.navigate([value]);
        this.handleclick.emit(value);
    }

}

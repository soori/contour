import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MegamenuComponent } from './megamenu.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [],
  declarations: [
  MegamenuComponent
  ],
  exports: [
  MegamenuComponent
  ]
})
export class MegamenuModule  {
}

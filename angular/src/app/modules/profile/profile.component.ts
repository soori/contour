
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './user';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'profile-cmp',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.scss']
})

export class ProfileComponent implements OnInit {

    public user: User;
    public profileName: string;
    @Input('profileInfo') profileInfo: any;

    constructor() {
    }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        if (this.user) {
            this.profileName = this.user.firstName.charAt(0);
        }
        this.user.menu = this.profileInfo;
    }
}

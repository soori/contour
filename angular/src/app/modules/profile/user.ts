import { Fav } from './fav';

export class User {
    userId: string;
    name: string;
    firstName: string;
    lastName: string;
    emailID: string;
    designation: string;
    department: string;
    timeZones: any[];
    defaultTimezone: string;
    image: string;
    // ideally it allow to update them independently :)
    favorites?: Fav[];
    variants?: any[];
    menu?: any[];
    roles?: any[];
    preferences?: any[];
    // TO BE DELETED
    assignedReports: any[];
    reviewReports: any[];
    departmentReports: any[];
}


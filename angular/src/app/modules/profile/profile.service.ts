import { User } from './user';
// Needs to be code refactor, its not right way

import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class ProfileService {

    constructor(private http: Http) {
    }

    loadProfileMenu() {
        return this.http.get('mock-data/bees/menu.json')
            .toPromise()
            .then(this.handleSuccess)
            .catch(this.handleError);
    }

    handleSuccess(response: any) {
        return response.json();
    }

    handleError(response: any) {
        console.log('response', response);
    }
}

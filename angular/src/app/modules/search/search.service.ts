import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Search } from './search';

@Injectable()
export class SearchService {
    public reportPath: string;
    public clientLogo: string;
    public searchData: any = './search.json';
    constructor(private http: Http) { }
    loadReports() {
        return this.http.get(this.searchData)
            .toPromise()
            .then(this.handleSuccess)
            .catch(this.handleError);
    }
    handleSuccess(response: any) {
        return response.json();
    }
    handleError(response: any) {
        console.log('response', response);
    }
}

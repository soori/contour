import {
    Component, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';
import 'rxjs/add/operator/filter';

@Component({
    selector: 'search-cmp',
    templateUrl: 'search.component.html',
    styleUrls: ['search.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchComponent {
    public showResult: boolean;
    public reportsInfo: any[];
    public reportsInfoFilter: any[];
    public searchText: string;
    public noResultFound: boolean;
    constructor() {
        this.showResult = false;
        this.noResultFound = false;

    }

    // get the results if user entered 3 letters in global search
    onKeyUp(event: any) {
        const searcValue = event.target.value;
        if (searcValue.length < 3) {
            this.showResult = false;
            return;
        }
        this.showResult = true;
        this.loadReports();
    }

    onBlur(event: any) {
        this.showResult = false;
        this.searchText = '';
    }

    onKeyEnter(event: any) {
        const searcValue = event.target.value;
        this.showResult = true;
        this.loadReports();
    }

    // loading reports in global search
    loadReports() {
        // this.searchService.loadReports().then(
        //     data => {
        //         this.reportsInfo = data.analystlist.companyList;
        //         this.reportsFilter(this.searchText);
        //     },
        //     err => { console.log('Error', err); }
        // );
    }

    // Filtering loaded data based on search value
    reportsFilter(searcValue: any) {
        // if (this.searchText !== undefined && this.searchText != null) {
        //     this.reportsInfoFilter = this.reportsInfo.filter(function (report:any) {
        //         return report['reportName'].toLowerCase().indexOf(this.searchText.toLowerCase()) > -1;
        //     }.bind(this));
        // } else {
        //     this.reportsInfoFilter = this.reportsInfo;
        // }
    }

    getReport(item: any) {
        this.showResult = false;
        this.searchText = '';
    }
}

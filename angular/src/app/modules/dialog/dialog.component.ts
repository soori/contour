import {
    Component, OnInit, Input, PipeTransform, Pipe, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'dialog-cmp',
    templateUrl: 'dialog.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['dailog.component.scss']
})
export class DialogComponent implements OnInit {
    @Input('modalPosition') modalPosition: any;
    ngOnInit() {
        if (this.modalPosition === undefined) {
            this.modalPosition = {
                modalId: 'largeShoes',
                modalSize: 'modal'
            };
        }
    }
}

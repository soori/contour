import { Component, OnInit, Input, OnChanges, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import * as moment_ from 'moment';
const moment = moment_;
import { stringify } from 'querystring';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';


@Component({
    moduleId: module.id,
    selector: 'app-simpleform-cmp',
    templateUrl: 'simpleform.component.html',
    styleUrls: ['simpleform.component.scss']
})


export class SimpleFormComponent implements OnInit, OnChanges {

    // Input
    @Input('formDataInput') formDataInput: any;

    // Output
    @Output() formSubmit: EventEmitter<any> = new EventEmitter();
    @Output() formReset: EventEmitter<any> = new EventEmitter();

    // Variables Declarations
    public value: Date;
    public formObject: any;
    public uservariantsInputData: any;
    public myForm: FormGroup;
    public singlePicker = { singleDatePicker: true, showDropdowns: true, opens: 'left' };
    public dropdownList: any;
    public dropdownSettings: any = {
        singleSelection: false,
        text: '',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: 'myclass custom-class',
        badgeShowLimit: 3,
        enableCheckAll: true
    };
    public selectedItems: any;
    public formDataInputCopy: any;
    public defaultItem: { DisplayLabel: string, DisplayValue: number } = { DisplayLabel: 'Please Select...', DisplayValue: null };


    constructor(private daterangepickerOptions: DaterangepickerConfig, private el: ElementRef) {
        this.daterangepickerOptions.settings = {
            alwaysShowCalendars: true,
            autoApply: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };
    }

    ngOnInit() {
    }

    ngOnChanges() {
        if (!!this.formDataInput && !!this.formDataInput.form && this.formDataInput.form.fields) {
            // default form componentsettings
            // show the form summary div
            this.formDataInput.showSummary =  this.formDataInput.hasOwnProperty('showSummary') ? this.formDataInput.showSummary : true;
            this.formDataInput.form.fields.forEach((eachObj: any) => {
                if (eachObj.type === 'singledatepicker') {
                    eachObj.defaultvalue = eachObj.defaultvalue === 'default' ? moment(new Date()).format(eachObj.format) : eachObj.defaultvalue;
                }
                if (eachObj.type === 'daterangepicker') {
                    eachObj.defaultvalue = eachObj.defaultvalue === 'default' ? moment(new Date()).format(eachObj.format) + '-' + moment(new Date()).format(eachObj.format) : eachObj.defaultvalue;
                }
                if (eachObj.type === 'multiselect') {
                    this.selectedItems = eachObj.defaultvalue;
                }
                eachObj.isDisabled = false;
            });

            this.formDataInputCopy = JSON.parse(JSON.stringify(this.formDataInput));
        }
        const jsonobject = {};
        if (this.formDataInput !== null && this.formDataInput !== undefined) {
            this.formDataInput.form.fields.forEach(function (field: any) {
                (jsonobject as any)[field['field']] = new FormControl(field['field']);
            });
            this.myForm = new FormGroup(jsonobject);
        }

    }

    submitForm(form: any) {
        // loop through the each field and get the Value
        this.formDataInput.form.fields.forEach((eachObj: any) => {
            if (eachObj.type === 'singledatepicker') {
                if (form.controls[eachObj.field].value === '') {
                    eachObj.value = '';
                } else {
                    const selectedDate = moment(form.controls[eachObj.field].value, eachObj.format);
                    eachObj.value = moment(selectedDate).format('YYYY-MM-DD');
                }
            } else if (eachObj.type === 'multiselect') {
                eachObj.value = this.selectedItems;
            } else {
                eachObj.value = form.controls[eachObj.field].value;
            }
        });
        this.formSubmit.emit();
        // console.log('form data:' + JSON.stringify(this.formDataInput.form.fields));
    }

    reset(data: any) {

        this.formDataInput = this.formDataInputCopy;
        this.formDataInputCopy = JSON.parse(JSON.stringify(this.formDataInput));
        this.selectedItems = [];
        console.log(this.formDataInputCopy);
        this.formReset.emit();

    }

    // SingleDatePicker Selected Event
    private singleSelect(value: any, formField: any) {
        formField.defaultvalue = moment(value.start).format(formField.format);
    }

    // DaterangeDatePicker Selected Event
    private selectedDate(value: any, formField: any) {
        formField.defaultvalue = moment(value.start).format(formField.format) + '-' + moment(value.end).format(formField.format);
    }

    focusOutFunction(formField: any) {
        if (formField.defaultvalue === 'T' || formField.defaultvalue === 't') {
            formField.defaultvalue = 'test';
        }
    }

    // Single Slect Filter Change Event
    handleFilter(value: any, formField: any) {
        const dropdwonData = this.formDataInputCopy.form.fields.find((s: any) => s.field === formField.field).ds.valuepool;
        formField.ds.valuepool = dropdwonData.filter((s: any) => s.DisplayLabel.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }

    // Checkbox Change Event for Actions
    onChange(formField: any) {
        let formObj = null;
        if (typeof formField.actions !== 'undefined') {
            for (let i = 0; i < formField.actions.fileds.length; i++) {
                formObj = this.formDataInput.form.fields.find((s: any) => s.field.trim() === formField.actions.fileds[i].trim());
                if (formField.defaultvalue === true) {
                    formObj.isDisabled = true;
                    // reset the control values
                    if (formObj.type === 'singledatepicker') {
                        formObj.defaultvalue = moment(new Date()).format(formObj.format);
                    } else if (formObj.type === 'daterangepicker') {
                        formObj.defaultvalue = moment(new Date()).format(formObj.format) + '-' + moment(new Date()).format(formObj.format);
                    } else if (formObj.type === 'singleselect') {
                        formObj.defaultvalue = this.formDataInputCopy.form.fields.find((s: any) => s.field.trim() === formField.actions.fileds[i].trim()).defaultvalue;
                    } else {
                        formObj.defaultvalue = '';
                    }
                } else {
                    formObj.isDisabled = false;
                }
            }

        }

    }

    applyRegExPattern(event: any, formField: any) {
        return String.fromCharCode(event.charCode).match(formField.pattern) != null;
    }

    // Multiselect Selected Events
    onItemSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }
    OnItemDeSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }
    onSelectAll(items: any) {
        console.log(items);
    }
    onDeSelectAll(items: any) {
        console.log(items);
    }

}

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { SimpleFormComponent } from '../simpleform/simpleform.component';
import { RouterModule } from '@angular/router';
import { DropDownListModule, DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { Daterangepicker } from 'ng2-daterangepicker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularMultiSelectModule,
    DropDownsModule,
    DropDownListModule,
    Daterangepicker
  ],
  providers: [],
  declarations: [
    SimpleFormComponent
  ],
  exports: [
    SimpleFormComponent
  ]
})
export class SimpleformModule  {
}

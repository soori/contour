import { Component, Input, OnInit, OnChanges, Output, EventEmitter, ViewContainerRef, ChangeDetectorRef, ElementRef, Renderer, HostListener, HostBinding, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

declare var bootbox: any;
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'app-userpreferences-cmp',
    templateUrl: 'userpreferences.component.html',
    styleUrls: ['userpreferences.component.scss']
})

export class UserPreferencesComponent implements OnInit,OnChanges {

    // Global Variables Declarations
    public UserpreferencesDataInputcopy: any = {};
    @Input() UserpreferencesDataInput: any = {};
    @Output() onSubmit: EventEmitter<any> = new EventEmitter();
    @Output() onReset: EventEmitter<any> = new EventEmitter();


    constructor() {
    }

    ngOnInit() {

    }

    ngOnChanges() {
        if (this.UserpreferencesDataInput != null) {
            this.UserpreferencesDataInputcopy = JSON.parse(JSON.stringify(this.UserpreferencesDataInput));
        } else {
            this.UserpreferencesDataInput = null;
        }

    }

    // Single Slect Filter Change Event
    handleFilter(value: any, formField: any) {
        const dropdwonData = this.UserpreferencesDataInputcopy.form.fields.find((s: any) => s.field === formField.field).ds.valuepool;
        formField.ds.valuepool = dropdwonData.filter((s: any) => s.DisplayLabel.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }

    save() {
        const formData = {};
        if (this.UserpreferencesDataInput.form.fields !== undefined) {
            this.UserpreferencesDataInput.form.fields.forEach(function (item) {
                formData[item.field] = item.defaultvalue === undefined || item.defaultvalue === '' ? '' : item.defaultvalue;
            });
        }
        this.onSubmit.emit(formData);
        console.log('Form Data to be emitted to component ===>' + JSON.stringify(formData));
    }


    reset() {
        this.onReset.emit();
    }
}

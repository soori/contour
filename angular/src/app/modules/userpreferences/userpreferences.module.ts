import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserPreferencesComponent } from '../userpreferences/userpreferences.component';
import { RouterModule } from '@angular/router';
import { DropDownListModule, DropDownsModule } from '@progress/kendo-angular-dropdowns';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    DropDownsModule,
    DropDownListModule
  ],
  providers: [],
  declarations: [
    UserPreferencesComponent
  ],
  exports: [
    UserPreferencesComponent
  ]
})
export class UserPreferencesModule  {
}

import {
    Component,
    OnInit,
    Input
} from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'panelheader-cmp',
    templateUrl: 'notificationpanelheader.component.html',
    styleUrls: ['notificationpanel.component.scss']
})
export class NotificationPanelHeaderComponent implements OnInit {
    @Input() headerData: any = {};
    public selectedValue: string;

    constructor() { }

    ngOnInit() {
        this.selectedValue = 'All';
    }
    viewChange(obj: any) {
        this.selectedValue = obj.displaylabel;
    }
}

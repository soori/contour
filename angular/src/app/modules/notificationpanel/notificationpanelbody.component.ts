import {
    Component,
    OnInit,
    Input
} from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'panelbody-cmp',
    templateUrl: 'notificationpanelbody.component.html',
    styleUrls: ['notificationpanel.component.scss']



})
export class NotificationPanelBodyComponent implements OnInit {
    @Input() notifications: any = [];

    public scrollbarOptions = { axis: 'y', theme: 'dark-3' };
    constructor() {
    }
    ngOnInit() {

    }
}

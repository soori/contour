import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationPanelComponent } from './notificationpanel.component';
import { NotificationPanelHeaderComponent } from './notificationpanelheader.component';
import { NotificationPanelBodyComponent } from './notificationpanelbody.component';
import { NotificationPanelFooterComponent } from './notificationpanelfooter.component';
import { NotificationModule } from '../notification/notification.module';

import { MalihuScrollbarModulee } from '../scrollbar';



@NgModule({
  imports: [
    CommonModule,
    NotificationModule,
    MalihuScrollbarModulee.forRoot()
  ],
  providers: [],
  declarations: [
    NotificationPanelComponent, NotificationPanelHeaderComponent, NotificationPanelBodyComponent, NotificationPanelFooterComponent
  ],
  exports: [
    NotificationPanelComponent, NotificationPanelHeaderComponent, NotificationPanelBodyComponent, NotificationPanelFooterComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class NotificationpanelModule {
}

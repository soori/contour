import {
    Component,
    Input
} from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'panelfooter-cmp',
    templateUrl: 'notificationpanelfooter.component.html',
    styleUrls: ['notificationpanel.component.scss']
})
export class NotificationPanelFooterComponent {
    @Input() footerData: any = {};

    constructor() {
    }

}

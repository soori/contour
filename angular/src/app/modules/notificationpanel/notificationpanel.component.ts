import { Component, Input, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'notification-panel',
    templateUrl: 'notificationpanel.component.html',
    styleUrls: ['notificationpanel.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,

})
export class NotificationPanelComponent {
    @Input() notificationPanelConfig: any = {};

    constructor() {
    }
}

import { Component, OnInit, Input, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'notification',
    templateUrl: 'notification.component.html',
    styleUrls: ['notification.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NotificationComponent implements OnInit {
    @Input() notification: any = {};

    constructor() { }
    ngOnInit() { }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { SearchModule } from '../search/search.module';
import { NotificationbellModule } from '../notificationbell/notificationbell.module';
import { FavoritesModule } from '../favorites/favorites.module';
import { MegamenuModule } from '../megamenu/megamenu.module';
import { ProfileModule } from '../profile/profile.module';
import { UservariantsModule } from '../uservariants/uservariants.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SearchModule,
    FavoritesModule,
    NotificationbellModule,
    FavoritesModule,
    MegamenuModule,
    ProfileModule,
    UservariantsModule

  ],
  providers: [],
  declarations: [
  HeaderComponent
  ],
  exports: [
  HeaderComponent
  ]
})
export class HeaderModule  {
}

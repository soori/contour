export class Header {
    projectName: string;
    navitems?: any[];
    logoutUrl?: string;
    custombar?: boolean;
    search?: boolean;
    variants?: boolean;
    notifications?: boolean;
    favorites?: boolean;
    profile?: boolean;
    navigations?: boolean;
    navigation?: any[];
}

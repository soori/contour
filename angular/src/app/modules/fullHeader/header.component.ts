
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Header } from './header';

@Component({
    moduleId: module.id,
    selector: 'header-cmp',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss']
})

export class HeaderComponent implements OnInit {

    @Input() header: Header;
    router: Router;

    @Output() menuitem = new EventEmitter<any>();

    constructor(router: Router) {
        this.router = router;
    }

    ngOnInit() {

    }

    signOut() {
        this.router.navigate(['logout']);
        location.reload();
    }

    onMenuChildClick(value: any) {
        this.router.navigate(['zmfs/' + value + '']);
    }
    handleUserUpdated(value: any) {
        this.menuitem.emit(value);
    }
    gotoHome() {
        this.router.navigate(['/FundSERVTransactions']);
    }
}

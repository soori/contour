import {
    Component, ViewEncapsulation,
    ChangeDetectionStrategy
} from '@angular/core';
import 'rxjs/add/operator/filter';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'notificationbell-cmp',
    templateUrl: 'notificationbell.component.html',
    styleUrls: ['notificationbell.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationbellComponent {
    constructor() {
    }
}

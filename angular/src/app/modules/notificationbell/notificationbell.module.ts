import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationbellComponent } from './notificationbell.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
  NotificationbellComponent
  ],
  exports: [
  NotificationbellComponent
  ]
})
export class NotificationbellModule  {
}

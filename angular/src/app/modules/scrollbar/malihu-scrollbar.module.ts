import { ModuleWithProviders } from '@angular/core';
import { NgModule } from '@angular/core';

import { MalihuScrollbarDirective } from './malihu-scrollbar.directive';
import { MalihuScrollbarService } from './malihu-scrollbar.service';

@NgModule({
  exports: [MalihuScrollbarDirective],
  declarations: [MalihuScrollbarDirective],
  providers: [MalihuScrollbarService],
})
export class MalihuScrollbarModulee {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MalihuScrollbarModulee,
      providers: [MalihuScrollbarService],
    };
  }
  static forChild(): ModuleWithProviders {
    return {
      ngModule: MalihuScrollbarModulee,
    };
  }
}

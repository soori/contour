import { Component } from '@angular/core';

@Component({
    selector: 'app-ontrackcard-cmp',
    templateUrl: 'ontrackcard.component.html',
    styleUrls: ['ontrackcard.component.scss']
})

export class OnTrackCardComponent {

    public cardObj = {
        'backgroundcolor': '#3CB541',
        'image': 'assets/green_smiley.png',
        'title': 'Everything is',
        'content': 'ON TRACK'
    };
    constructor() {
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnTrackCardComponent } from './ontrackcard.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    OnTrackCardComponent,
  ],
  exports: [
    OnTrackCardComponent,
  ]
})
export class OnTrackCardModule  {
}

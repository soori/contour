import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimerCardComponent } from './timercard.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    TimerCardComponent,
  ],
  exports: [
    TimerCardComponent,
  ]
})
export class TimerCardModule {
}

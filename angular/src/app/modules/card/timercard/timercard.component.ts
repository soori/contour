import {
    Component, OnInit, Input
} from '@angular/core';
import { Router } from '@angular/router';
import * as momenttimezone from 'moment-timezone';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'timercard-cmp',
    templateUrl: 'timercard.component.html',
    styleUrls: ['timercard.component.scss']
})

export class TimerCardComponent implements OnInit {
    public timeZone: any = {};
    public interval: any;
    @Input('timeZones') timeZones: any;
    @Input('defaultTimeZone') defaultTimeZone: any;

    public myDate: Date;
    public minutes: string;
    public CurrentTime: any;
    public CurrentTimeAMPM: any;
    public cardObj = {
        'backgroundcolor': '#007DB2',
        'image': 'assets/time.png'
    };
    constructor(private router: Router) {
    }
    ngOnInit() {
        this.utcTime(this.defaultTimeZone);
    }
    // Get current date time
    utcTime(timeZone: any): void {
        this.getDateTime(timeZone);
        this.interval = setInterval(() => {
            this.getDateTime(timeZone);
        }, 10000);
    }

    // get date object based on selected time zone
    getDateTime(timeZone: any) {
        this.myDate = new Date(new Date().toString());
        switch (timeZone) {
            case 'GMT': {
               const timezoneTime = this.toTimeZone(this.myDate, 'Europe/London');
               this.CurrentTime =  timezoneTime.toString().split(' ')[0];
               this.CurrentTimeAMPM =  timezoneTime.toString().split(' ')[1];
                break;
            } case 'EST': {
               const timezoneTime = this.toTimeZone( this.myDate, 'America/New_York');
               this.CurrentTime =  timezoneTime.toString().split(' ')[0];
               this.CurrentTimeAMPM =  timezoneTime.toString().split(' ')[1];
                break;
            } case 'IST': {
                const timezoneTime = this.toTimeZone(this.myDate, 'Asia/Kolkata');
                this.CurrentTime =  timezoneTime.toString().split(' ')[0];
                this.CurrentTimeAMPM =  timezoneTime.toString().split(' ')[1];
                break;
            }
            default: {
                const timezoneTime = this.toTimeZone( this.myDate, 'Asia/Kolkata');
                this.CurrentTime =  timezoneTime.toString().split(' ')[0];
                this.CurrentTimeAMPM =  timezoneTime.toString().split(' ')[1];
                break;
            }
        }
    }

    // IE is not supproting angular pipes, so manually getting the minutes from time object.
    // Get minutes
    getMinutes(date: any) {
        let separater = ':';
        if (date.getMinutes() < 10) {
            separater = ':0';
        } if (date.getMinutes() === 0) {
            separater = ':0';
        }
        return this.minutes = separater + date.getMinutes();
    }

    // Time zone change
    timeZoneChange(Obj: any) {
        this.defaultTimeZone = Obj;
        clearInterval(this.interval);
        this.utcTime(Obj);
    }

    toTimeZone(time: any, zone: any) {
        const format = 'h:mm a';
        return momenttimezone(time, format).tz(zone).format(format);
    }
}

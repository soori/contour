import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  CardComponent, CardTitle, CardIcon, CardContent, CardStutus, CardHeader, CardFooter, CardActions,
  CardSmIcon, CardMdIcon, CardLgIcon, CardSubTitle, CardModal
} from './card.component';
import { OnTrackCardComponent } from './ontrackcard/ontrackcard.component';
import { ProfileCardComponent } from './profilecard/profilecard.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    CardComponent, CardTitle, CardIcon, CardContent,
    CardStutus, CardHeader, CardFooter, CardActions,
    CardSmIcon, CardMdIcon, CardLgIcon, CardSubTitle,
    CardModal, OnTrackCardComponent, ProfileCardComponent
  ],
  exports: [
    CardComponent, CardTitle, CardIcon, CardContent,
    CardStutus, CardHeader, CardFooter, CardActions,
    CardSmIcon, CardMdIcon, CardLgIcon, CardSubTitle
    , CardModal, OnTrackCardComponent, ProfileCardComponent
  ]
})
export class CardModule {
}

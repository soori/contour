import {
  Component,
  Directive,
  Input, OnInit, ViewEncapsulation,
  ChangeDetectionStrategy,
} from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

// card component
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'card-cmp',
  templateUrl: 'card.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['card.component.scss'],
  // tslint:disable-next-line:use-host-property-decorator
  host: { 'class': 'ct-card-cmp ct-card card card-block' }
})

export class CardComponent implements OnInit {
  modalClass: boolean;
  constructor() {
    this.modalClass = false;
  }
  ngOnInit() {
  }
}

// card header

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'card-header',
  templateUrl: 'card.header.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line:use-host-property-decorator
  host: { 'class': 'card-header' }
})
// tslint:disable-next-line:component-class-suffix
export class CardHeader { }

// Card footer

@Component({

  // tslint:disable-next-line:component-selector
  selector: 'card-footer',
  templateUrl: 'card.footer.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line:use-host-property-decorator
  host: { 'class': 'card-footer' }
})
// tslint:disable-next-line:component-class-suffix
export class CardFooter { }
// Card modal
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'card-modal',
  templateUrl: 'card.modal.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line:use-host-property-decorator
  host: { 'class': 'card-model' },
})
// tslint:disable-next-line:component-class-suffix
export class CardModal {
}


// card title

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-title',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-title'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class CardTitle { }

// card sub title

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-sub-title',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-sub-title'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class CardSubTitle { }

// Card content
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-content',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-text'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class CardContent { }

// Card status

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-status',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-stutus'
  }
})

// tslint:disable-next-line:directive-class-suffix
export class CardStutus { }

// Card image
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-icon',
   // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-icon'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class CardIcon { }

// Card Small  image
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-sm-icon',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-sm-icon'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class CardSmIcon { }

// Card Medium  image
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-md-icon',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-md-icon'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class CardMdIcon { }

// Card Large  image
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-lg-icon',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-lg-icon'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class CardLgIcon { }

// Card actions

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'card-actions',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'card-actions',
  }
})
// tslint:disable-next-line:directive-class-suffix
export class CardActions { }





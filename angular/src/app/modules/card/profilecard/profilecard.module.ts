import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileCardComponent } from './profilecard.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    ProfileCardComponent,
  ],
  exports: [
    ProfileCardComponent,
  ]
})
export class ProfileCardModule  {
}

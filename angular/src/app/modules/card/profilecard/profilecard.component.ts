import {
    Component, OnInit, Input
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'profile-card-cmp',
    templateUrl: 'profilecard.component.html',
    styleUrls: ['profilecard.component.scss']
})

export class ProfileCardComponent implements OnInit {
    public user: any;
    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
    }
}

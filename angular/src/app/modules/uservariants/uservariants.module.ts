import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoadUserVariantsComponent } from './loaduservariants.component';
import { UserVariantsService } from './uservariants.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  providers: [UserVariantsService],
  declarations: [
    LoadUserVariantsComponent,
  ],
  exports: [
    LoadUserVariantsComponent,
  ]
})
export class UservariantsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UservariantsModule,
      providers: [UserVariantsService],
    };
  }
}

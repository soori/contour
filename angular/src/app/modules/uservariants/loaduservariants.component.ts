import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Variants } from './variants';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'loaduservariants-cmp',
    templateUrl: 'loaduservariants.component.html',
    styleUrls: ['uservariants.component.scss']
})
export class LoadUserVariantsComponent {

    userVariants: Variants[];
    @Input('variantsInfo') variantsInfo: any;
    constructor(private router: Router) {
    }
    navigateVariants(value: any, name: any) {
        this.router.navigate([value], { queryParams: { variantname: name } });
    }
    loadUserVariants() {
        this.userVariants = this.variantsInfo;
    }
}

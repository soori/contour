export class UserVariants {
    type: string;
    userid: string;
    variantId: string;
    variantName: string;
    url: string;
    runImmediate: boolean;
    addMenu: boolean;
    addFav: boolean;
    searchFields: { name: string, value: string, display: boolean }[];
    gridFields: { name: string, value: string }[];
}


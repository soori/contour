import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Variants } from './variants';

@Injectable()
export class UserVariantsService {

    constructor(private http: Http) { }

    loaduserVariants(): Observable<any | Variants[]> {
        if (localStorage.getItem('userVariants')) {
            const fav: Variants[] = JSON.parse(localStorage.getItem('userVariants'));
            return Observable.of(fav);
        } else {
            return this.http.get('mock-data/favorites.json')
                .map((res: Response) => {
                    const fav: Variants[] = res.json();
                    if (fav) {
                        localStorage.setItem('userVariants', JSON.stringify(fav));
                    }
                    return fav;
                }).catch((err) => {
                    return Observable.empty();
                });
        }
    }

    saveuserVariants(variant: Variants) {
        if (localStorage.getItem('userVariants')) {
            const userVariantsList: any = JSON.parse(localStorage.getItem('userVariants'));
            const isExist: Variants[]  = userVariantsList.filter((item: any) => item.name === variant.name);
            if (isExist.length > 0) {
                // TODO :: make async service call to update
            } else {
                // TODO :: make async service call to update
                userVariantsList.push(variant);
            }
            localStorage.setItem('userVariants', JSON.stringify(userVariantsList));
            return true;
        }
    }
}

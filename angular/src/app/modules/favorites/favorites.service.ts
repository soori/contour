import { Fav } from './fav';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';


@Injectable()
export class FavoritesService {
    constructor(private http: Http) { }
    loadFavorites(): Observable<any | Fav[]> {
        if (localStorage.getItem('userFavourites')) {
            const fav: Fav[] = JSON.parse(localStorage.getItem('userFavourites'));
            return Observable.of(fav);
        } else {
            return this.http.get('./favorites.json')
                .map((res: Response) => {
                    const fav: Fav[] = res.json();
                    if (fav) {
                        localStorage.setItem('userFavourites', JSON.stringify(fav));
                    }
                    return fav;
                }).catch((err: any) => {
                    return Observable.empty();
                });
        }
    }

    updateFavourites(favorite: Fav) {
        if (localStorage.getItem('userFavourites')) {
            const favouritesList: Fav[] = JSON.parse(localStorage.getItem('userFavourites'));
            const isExist: Fav[] = favouritesList.filter(item => item.name === favorite.name);
            if (isExist.length > 0) {
                // TODO :: make async service call to delete
                for (let i = 0; i < favouritesList.length; i++) {
                    if (favouritesList[i].name === favorite.name) {
                        favouritesList.splice(i, 1);
                    }
                }
            } else {
                // TODO :: make async service call to update
                favouritesList.push(favorite);
            }
            localStorage.setItem('userFavourites', JSON.stringify(favouritesList));
        }
    }

    isFavourite(favorite: Fav): boolean {
        if (localStorage.getItem('userFavourites')) {
            const fav: Fav[] = JSON.parse(localStorage.getItem('userFavourites'));
            const isExist: Fav[] = fav.filter(item => item.name === favorite.name);
            if (isExist.length > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}

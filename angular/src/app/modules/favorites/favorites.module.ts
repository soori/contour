import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders } from '@angular/core';
import { FavoritesComponent } from './favorites.component';
import { FavoritesService } from './favorites.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [FavoritesService],
  declarations: [
    FavoritesComponent
  ],
  exports: [
    FavoritesComponent
  ]
})
export class FavoritesModule {
   static forRoot(): ModuleWithProviders {
    return {
      ngModule: FavoritesModule,
      providers: [FavoritesService],
    };
  }
}

import { Fav } from './fav';
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'favorites-cmp',
    templateUrl: 'favorites.component.html',
    styleUrls: ['favorites.component.scss']
})
export class FavoritesComponent {

    allfavourites: Fav[];
    @Input('favoritesInfo') favoritesInfo: any;
    constructor(private router: Router, ) {
    }
    navigateFavourite(value: any) {
        this.router.navigate([value]);
    }
    // load favorites from input data
    loadFavorites() {
        this.allfavourites = this.favoritesInfo;
    }
}

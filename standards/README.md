# Contour

Contour is a Broadridge UI guidelines project , bootstrap extended components,that are mobile-ready , desktop-ready and  for web applications

### Software pre requisites
* [node.js] - evented I/O for the backend
* [Gulp] - the streaming build system
  - Gulp (installed globally)
* [Bower] - a package manager for the web
  - Bower (installed globally,* required to to pull latest bootstrap source file)
  
### Installation:

Install the dependencies and devDependencies and start the server.
```sh
  $ npm instal
```
Build the artifacts and launch the documentation page on watch mode

```sh
  $ gulp
```
> To get the latest bootstrap source files from  the repository
> The idea is that a countour customization is 0 day old
> contour should be greatest & latest all the time

### Plugins

Dillinger is currently extended with the following plugins

* Babel
* Scss-lint
* Uglify
* Brower-sync

### Todos

 - Write Tests
 - Rethink Github pull
 - Bootstrap version configuration
 - Independent component artifact

Broadridge License 


**Broadridge Software**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   
   [node.js]: <http://nodejs.org>
   [Gulp]: <http://gulpjs.com>
   [Bower]: <https://bower.io/>


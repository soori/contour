0.0.1 -dev
release date : 01-12-2017
Initial commit

0.0.2 -dev
release date : 12-12-2017

Added more images
Megamenu
Style refactoring for components

1.0.0 -dev
release date : 22-01-2018

Bootstrap version upgraded and build changes made
